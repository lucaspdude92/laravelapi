@extends('layouts.app')
@section('content')


@foreach ($solicitacoes as $solicitacao)
<div class="card" style="margin:0px 0px 15px 0px">
    <div class="card-body">
      <h5 class="profile-title">
          <a href="">
            <img src="https://i.picsum.photos/id/65/200/200.jpg" alt="" class="profile-title-img">
            {{$solicitacao->user->name}}
          </a>
          <span class="float-right">{{$solicitacao->created_at->diffForHumans()}}</span>
        </h5>
<hr>
        <p class="card-text">
            {{$solicitacao->descricao}}
        </p>
            @foreach ($solicitacao->categorias as $categoria )
                Categoria: <a href="">{{$categoria->nome}}</a>
            @endforeach

        <div class="float-right">
            <a href="{{route('solicitacoes.show', $solicitacao->id)}}" class="btn btn-sm btn-primary">Ver <i class="fas fa-eye"></i></a>
            <a href="{{route('solicitacoes.show', $solicitacao->id)}}" class="btn btn-sm btn-info">Responder <i class="fas fa-reply"></i></a>

        </div>
      <div class="clearfix"></div>
    </div>
  </div>


@endforeach


{{$solicitacoes->links()}}



@endsection
