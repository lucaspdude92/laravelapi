@extends('layouts.app')
@section('content')



@if ($message = Session::get('success'))

<div class="alert alert-success alert-block">

	<button type="button" class="close" data-dismiss="alert">×</button>

        <strong>{{ $message }}</strong>

</div>

@endif


<div class="card" style="margin:0px 0px 15px 0px">
    <div class="card-body">
      <h5 class="profile-title">
          <a href="">
            <img src="https://i.picsum.photos/id/65/200/200.jpg" alt="" class="profile-title-img">
            {{$solicitacao->user->name}}
          </a>
          <span class="float-right">{{$solicitacao->created_at->diffForHumans()}}</span>
        </h5>
<hr>
        <p class="card-text">
            {{$solicitacao->descricao}}
        </p>
            @foreach ($solicitacao->categorias as $categoria )
                Categoria: <a href="">{{$categoria->nome}}</a>
            @endforeach


        <p class="card-text">
            {{$solicitacao->info}}
        </p>
        <p class="card-text">
            Prazo: {{$solicitacao->prazo}}
        </p>

        <hr>
        <div class="float-right">


            <a class="btn btn-sm btn-primary" data-toggle="modal" data-target="#modal-resposta-{{$solicitacao->id}}">
                Responder <i class="fas fa-reply"></i>
            </a>



        </div>
      <div class="clearfix"></div>
    </div>
  </div>



  <div class="modal fade" id="modal-resposta-{{$solicitacao->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">Enviar Resposta</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{route('resposta.enviar',$solicitacao->id)}}" method="POST">
        <div class="modal-body">



            <div class="form-group">
                <label for="mensagem" class="">{{ __('Mensagem') }}</label>
                    <textarea name="mensagem" id="mensagem"  rows="3" class="form-control"></textarea>
                    @error('mensagem')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
            </div>

            <div class="form-group">
                <label for="valor" class="">{{ __('Valor') }}</label>
                <input type="text" name="valor" id="valor" class="form-control">
                    @error('valor')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
            </div>


            @csrf
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="submit" class="btn btn-primary">Enviar Resposta</button>
        </div>
    </form>
      </div>
    </div>
  </div>

@endsection
