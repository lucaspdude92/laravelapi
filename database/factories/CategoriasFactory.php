<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Categorias;
use App\Model;
use Faker\Generator as Faker;

$factory->define(Categorias::class, function (Faker $faker) {
    return [
        'nome' => $faker->sentence(3)
    ];
});
