<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Solicitacoes;
use Faker\Generator as Faker;

$factory->define(Solicitacoes::class, function (Faker $faker) {
    return [
        'descricao' => $faker->sentence,
        'ativo' => true,
        'info' => $faker->paragraph,
        'prazo' => $faker->dateTimeBetween('now', '+1 month'),

    ];
});
