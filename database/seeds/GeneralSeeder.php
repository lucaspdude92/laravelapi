<?php

use App\Categorias;
use App\Solicitacoes;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GeneralSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categorias = factory(Categorias::class, 5)->create();


        $usuarios = factory(User::class, 5)->create()->each(function($usuario){
            $solicitacoes = factory(Solicitacoes::class, 5)->create([
                'user_id' => $usuario->id
            ])->each(function($solicitacao){
                $categoria = Categorias::find(rand(1,10));
                $solicitacao->categorias()->sync($categoria);
            });

        });
    }
}
