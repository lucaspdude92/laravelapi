<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::resource('solicitacoes', 'SolicitacoesController');
Route::get('solicitacoes?categoria={categoria}', 'SolicitacoesController@index')->name('solicitacoes.categorias');
Route::resource('categorias', 'CategoriasController');


Route::post('solicitacoes/{id}/responder', 'RespostasController@resposta')->name('resposta.enviar');


Auth::routes();
