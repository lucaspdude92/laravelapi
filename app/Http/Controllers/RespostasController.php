<?php

namespace App\Http\Controllers;

use App\Respostas;
use App\Solicitacoes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RespostasController extends Controller
{

    public function resposta($id, Request $request){

        $resposta = new Respostas();
        $resposta->user_id = Auth::user()->id;
        $resposta->solicitacoes_id = $id;
        $resposta->mensagem = $request->mensagem;
        $resposta->valor = $request->valor;
        $resposta->save();

        return redirect()->back()->with('success', 'Resposta enviada com sucesso');

    }

}
