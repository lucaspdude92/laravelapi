<?php

namespace App\Http\Controllers;

use App\Solicitacoes;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {

        $solicitacoes = Solicitacoes::paginate(50);
        return view('home')->with('solicitacoes', $solicitacoes);
    }
}
